#!/usr/bin/python3
import os

import lifecycle_msgs.msg
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node, LifecycleNode
from launch.substitutions import LaunchConfiguration
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    decoder_node = LifecycleNode(package='lslidar_n301_decoder',
                                executable='lslidar_n301_decoder_node',
                                name='lslidar_n301_decoder_node',		#设置激光数据topic名称
                                output='screen',
                                emulate_tty=True,
                                namespace='',
                                parameters=[
                                    {'child_frame_id': 'laser_link'},	#激光坐标
                                    {'point_num': 2000},
                                    {'angle_disable_min': 0.0},		#雷达裁剪角度开始值
                                    {'angle_disable_max': 0.0},		#雷达裁剪角度结束值
                                    {'min_range': 0.0},			#雷达最小距离
                                    {'max_range': 100.0},			#雷达最远距离
                                    {'frequency': 10.0},			#雷达频率
                                    {'use_gps_ts': False},			#GPS使用
                                    {'gps_correct': True},
                                    {'publish_point_cloud': True},
                                    {'filter_scan_point': True},
                                    {'agreement_type': 1} 			#<!--  1-1.7协议   2-1.6协议   -->
                                    ])
                                
    driver_node = LifecycleNode(package='lslidar_n301_driver',
                                executable='lslidar_n301_driver_node',
                                name='lslidar_n301_driver_node',		#设置激光数据topic名称
                                output='screen',
                                emulate_tty=True,
                                namespace='',
                                parameters=[ 
                                    {'frame_id': 'laser_link'},
                                    {'device_ip': '192.168.1.225'},		#IP 2368 192.168.1.222 LIDAR
                                    {'device_port': 2368},			#端口    2369 192.168.1.225 DEVICE
                                    {'add_multicast': False},		#组播
                                    {'group_ip': '224.1.1.2'}
                                ])
                                
    return LaunchDescription([
        decoder_node,
        driver_node,
    ])

